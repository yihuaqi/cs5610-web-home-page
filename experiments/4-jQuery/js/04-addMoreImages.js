﻿$(function () {
    var imgs = new Array();
    $(".addImageBtn").click(addImg);


    function addImg() {
        
        var imgURL = $(".image-url").val();
        var imgLi = $("<li>").append(($("<img />").attr({ "src": imgURL, "width": "100%" })));
        var slider = $('.flexslider').data('flexslider');
        slider.addSlide($(imgLi));

    }


    $('.flexslider').flexslider({
        //We must have these features to correctly add new slide. Magic.
        animation: "slide",
        animationLoop: false,
    });


});