﻿$(function () {
    var currentIndex = 0;
    var imgs = new Array();
    imgs[0] = "http://t0.gstatic.com/images?q=tbn:ANd9GcRX1FexG5zTUkwLCgQ46E55Rrj4sH2MUFrpJa_r4HysjKOtyghz";
    imgs[1] = "http://news.sfacg.com/Images/Inner/07dd115b-cb07-40e4-ba64-f584b5b62ccd.jpg";
    imgs[2] = "http://comic.sun0769.com/upload/comic/image/20140214/20140214105687968796.jpg";
    var timeInterval = 2000;
    setInterval(changeImg, timeInterval);

    populateImgs();
    function changeImg() {
        var portrait = $(".portrait");
        if (currentIndex == imgs.length - 1) {
            currentIndex = 0;
        } else {
            currentIndex++; 
        }
        portrait.attr("src", imgs[currentIndex]);
    }

    function populateImgs() {
        var $imgs = $(".slides");
        for (var i = 0; i < imgs.length; i++){
            $imgs.append($("<li>").append($("<img />").attr({"src": imgs[i],"width":"100%"})));
            
        }
    }
    $(window).load(function () {
        $(".flexslider").flexslider();
    });
});