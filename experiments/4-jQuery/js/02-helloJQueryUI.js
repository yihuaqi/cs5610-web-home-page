﻿$(function () {
    $(".draggable").draggable();
    $(".droppable").droppable({
        drop: function (event, ui) {
            
            $(this).addClass("highlighted").html(ui.draggable.context.innerText);
        }
    });
    $(".resizable").resizable();
    $(".selectable").selectable({
        selected: function (event, ui) {
            console.log(ui);
            //ui.addClass("highlighted");
        },
        unselected: function (event, ui) {
            console.log("unselected");
            //ui.removeClass("highlighted");
        }
    });
    $(".sortable").sortable();
})