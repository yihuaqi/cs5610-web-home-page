﻿$(document).ready(function () {
    console.log("Ready");
    $("#toggle-to-hightlight").click(function () {
        $(this).toggleClass("highlighted");
    });

    $("#toggle-to-changecontent").click(function () {
        $(this).html("You did it!");
    });

    $("#button").click(function () {
        var toAdd = $('input[name=checkListItem]').val();
        $(".list").append('<div class="item">' + toAdd + '</div>');
    });

    $(document).on('click', '.item', function () {
        $(this).remove();
    });


})