﻿$(document).ready(function () {
    console.log("ready!");


    $('div.enterToFade').mouseenter(function () {
        $('div.enterToFade').fadeTo('fast', 1);
    });
    $('div.enterToFade').mouseleave(function () {
        $('div.enterToFade').fadeTo('fast', 0.5);
    });

    $('div.clickToFade').click(function () {
        $('div.clickToFade').fadeOut('slow');
    });

    $('div.clickToChangeText').click(function () {
        
        $('div.clickToChangeText').text("You Did It!");
        
    });

    $('div.clickToFadeThis').click(function () {
        
        $(this).fadeOut('slow');
    });

    $('.pull-me').click(function () {
        $('.panel').slideToggle('slow');
    });
});