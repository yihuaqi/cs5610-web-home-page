﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class experiments_6_ASP_02_customizedLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void loginBtn_Click(object sender, EventArgs e)
    {
        string username = usernameTxtbx.Text;
        string password = passwordTxtbx.Text;
        if (username == null)
        {
            statusLable.Text = "Please Input Username: Hello";
        }
        else if (password == null)
        {
            statusLable.Text = "Please Input Password: World";
        }
        else if (username == "Hello" && password == "World")
        {
            statusLable.Text = "You are logged In!";
            Session["loggedIn"] = username;
        }
        else
        {
            statusLable.Text = "Username:Hello     Password:World";
        }
    }
    protected void logoutBtn_Click(object sender, EventArgs e)
    {
        Session["loggedIn"] = null;
        statusLable.Text = "You are logged out";
    }
}