﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
public partial class experiments_6_ASP_06_accessDatabaseByAjax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod]
    public static Example[] getExamples()
    {
        
        List<Example> examples = new List<Example>();
        
        string sql = "SELECT * FROM [Example]";
        string connectionString = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        SqlCommand command = new SqlCommand(sql, connection);
        SqlDataReader reader = command.ExecuteReader();
        while (reader.Read())
        {
            Example example = new Example();
            example.Id = reader.GetInt32(0);
            example.ExampleId = reader.GetString(1);
            example.Name = reader.GetString(2);
            examples.Add(example);
        }

        connection.Close();


        return examples.ToArray();
    }
}