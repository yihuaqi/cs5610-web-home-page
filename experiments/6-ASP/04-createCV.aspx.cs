﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class experiments_6_ASP_04_createCV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void cv_submit_btn_Click(object sender, EventArgs e)
    {
        cv_name_literal.Text = cv_name_textbox.Text;
        cv_age_literal.Text = cv_age_textboxp.Text;
        cv_gender_literal.Text = cv_gender_textbox.Text;
        cv_description_literal.Text = cv_description_textbox.Text;
        string imagesString = Request.Form["cv_images_input"];
        string[] images = imagesString.Split(new string[] { "???" }, StringSplitOptions.RemoveEmptyEntries);
        string images_literal = "";
        for(int i = 0; i < images.Count(); i++){
            images_literal += images[i]+"<br />";
        }
        
        cv_images_literal.Text = images_literal;

    }

}