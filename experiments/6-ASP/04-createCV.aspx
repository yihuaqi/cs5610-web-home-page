﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="04-createCV.aspx.cs" Inherits="experiments_6_ASP_04_createCV" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../../js/jquery-1.11.1.js"></script>
    <script src="../../js/jquery-ui.js"></script>

    <link href="../../css/jquery-ui.structure.css" rel="stylesheet" />

    <script src="js/04-createCV.js"></script>
    <link href="../../css/bootstrap.css" rel="stylesheet" />
    <link href="../css/experiments.css" rel="stylesheet" />
    <link href="css/04-createCV.css" rel="stylesheet" />
    <script src="../../js/jquery.flexslider.js"></script>
    <link rel="stylesheet" href="../../css/flexslider.css" type="text/css" />
    <title></title>
</head>
<body>
    
    
        <div class="demo-detail">
            <h1 class="demo-title">Delete Images</h1>
            <p class="demo-detail">This experiment create a profile of the voice actress.</p>
            <div class="voiceActress">
            
                <div class="portrait-wrapper">
                    <div class="flexslider">

                        <ul class="slides">
                            <!-- We must need a placeholder here to be able to add image. Magic. -->
                            <li>
                                <img class="portrait selectDisable cv-img" src="http://upload.wikimedia.org/wikipedia/commons/a/a1/Kana_Hanazawa.jpg" />
                                
                            </li>

                        </ul>
                        
                        

                        <!-- A delete button for deleting the current image. -->
                        <img class="deleteBtn inactive selectDisable" src="../../images/project/delete.png" />
                    </div>
                    URL<input class="image-url form-control"/>
                    <button class="btn btn-success addImageBtn">Add</button>
                    
                </div>
        <form id="form1" runat="server">

            <input type="hidden" id="cv_images_input" name="cv_images_input" value="" />
                <div class="voiceActress-description">
                
                    <p>
                        <span class="description-title">Name:</span> <span id="cv-name" class="description-content" contenteditable="true">Kana Hanazawa</span>
                        <asp:TextBox ID="cv_name_textbox" runat="server"></asp:TextBox>
                    </p>

                    
                    <hr />
                    <p>
                        <span class="description-title">Gender:</span> <span id="cv-gender" class="description-content" contenteditable="true">Female</span>
                        <asp:TextBox ID="cv_gender_textbox" runat="server"></asp:TextBox>
                    </p>
                    
                    <hr />
                    <p>
                        <span class="description-title">Age:</span> <span id="cv-age" class="description-content" contenteditable="true">28</span>
                        <asp:TextBox ID="cv_age_textboxp" runat="server"></asp:TextBox>
                    </p>
                    
                    <hr />
                    <p>
                        <span class="description-title">Description:</span><span id="cv-description" class="description-content" contenteditable="true">
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                            non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                        </span>
                        <asp:TextBox ID="cv_description_textbox" runat="server"></asp:TextBox>
                    </p>
                    
                </div>
            <asp:Button ID="cv_submit_btn" runat="server" Text="Submit" OnClick="cv_submit_btn_Click" />

            <ul>
                <li>
                    <asp:Literal ID="cv_name_literal" runat="server"></asp:Literal>
                </li>
                <li>
                    <asp:Literal ID="cv_age_literal" runat="server"></asp:Literal>
                </li>
                <li>
                    <asp:Literal ID="cv_gender_literal" runat="server"></asp:Literal>
                </li>
                <li>
                    <asp:Literal ID="cv_description_literal" runat="server"></asp:Literal>
                </li>

                <li>
                    <asp:Literal ID="cv_images_literal" runat="server"></asp:Literal>
                </li>
            </ul>



            </form>
            </div>
            
            <h1>Documentation</h1>
            <p class="demo-detail">
                In this experiment, I tried to use aspx to post the information of a CV to the server.
                <br />
                Here we use textbox to post the information to the server.
                <br />
                <pre>
    In aspx:

    &lt;asp:TextBox ID=&quot;TextBox1&quot; runat=&quot;server&quot;&gt;&lt;/asp:TextBox&gt;

    and in C#

    cv_name_literal.Text = cv_name_textbox.Text;
                </pre>
                A very tricky part is to post all the images to server.
                <br />
                My first trial was to replace all the elements in the flexslider to the equavalent asp controller.
                But soon I realized no one is using flexslider and asp.net together.
                <br />
                Then I tried to use the hidden field which is a usual way to communicate between client and server.
                But then I found that every time i tried to set the value of hiddenfield, the page refrehsed.
                <br />
                I was wondering if there is any way that I could store the value to hidden field without reloading the page.
                Then I found that there is something called updatepanel that can refresh part of the page.
                <br />
                So I tried that, didn't work.
                Maybe I was on the wrong track, but this seemed ridiculous to me that such a simple task can not be achieved.
                <br />
                Then I thought of that the nature of communication between client and server using ASP.NET is by submitting form.
                So Why don't I add some input field and submit it along with form?
                <pre>
    In aspx:
    &lt;input type=&quot;hidden&quot; id=&quot;cv_images_input&quot; name=&quot;cv_images_input&quot; value=&quot;&quot; /&gt;

    In C#:
    string imagesString = Request.Form[&quot;cv_images_input&quot;];
                </pre>
                But one input text can only pass one string. How do I pass an array of strings?
                Well, I can use some kind of delimiter to concatenate all the strings.
                <pre>
    JS:
    $(&quot;.cv-img&quot;).each(function () {
            
        imgSrcs += $(this).attr(&apos;src&apos;) + &quot;???&quot;;
    });

    In C#:
    string[] images = imagesString.Split(new string[] { &quot;???&quot; }, StringSplitOptions.RemoveEmptyEntries);
                </pre>
                It's ugly but still work.

                <br />
                Maybe I should use a javascript function to modify the form on submit, and pass a Json to the server.
                <a href="http://stackoverflow.com/questions/19293125/asp-net-post-data-from-client-to-server">Handle onSubmit</a>
            </p>

        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/04-createCV.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/04-createCV.aspx.cs" target="_blank">cs file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/js/04-createCV.js" target="_blank">js file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/css/04-createCV.css" target="_blank">css file</a></li>
        </ul>

        </div>
    

</body>
</html>
