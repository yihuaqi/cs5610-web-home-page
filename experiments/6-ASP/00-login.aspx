﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="00-login.aspx.cs" Inherits="experiments_6_ASP_00_login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="../../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/experiments.css" />
    <title>00-Login</title>
</head>
<body>
    
    <div class="container">
        <h1>Login</h1>
        <form id="form1" runat="server">
    <div>
    
    </div>
        <asp:Login ID="Login1" runat="server" OnAuthenticate="Login1_Authenticate">
        </asp:Login>
    </form>
            <p class="demo-detail">
                To use the login widget provided by VS, we first add a new Web Form to generate an aspx file.
                Then we go to the design perspective, drag the login widget from Toolbox.
            </p>
        <br />
        <p class="demo-detail">
                Then double click the login widget, "protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)" is automatically generated.
                <pre>
    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
        if (Login1.UserName == "Hello" && Login1.Password == "World")
        {
            e.Authenticated = true;
        }
    }
                </pre>
            Check the username and password, and set e.Authenticated = true will finish the authentication.
            </p>
        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/00-login.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/00-login.aspx.cs" target="_blank">cs file</a></li>
            
        </ul>
    </div>
</body>
</html>
