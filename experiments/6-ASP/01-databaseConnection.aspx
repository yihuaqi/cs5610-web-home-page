﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="01-databaseConnection.aspx.cs" Inherits="experiments_6_ASP_databaseConnection" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Database Connection</title>
    <link rel="stylesheet" href="../../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/experiments.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>Database Connection</h1>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="Example" HeaderText="Example" SortExpression="Example" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Example] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Example] ([Example], [Name]) VALUES (@Example, @Name)" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Id], [Example], [Name] FROM [Example]" UpdateCommand="UPDATE [Example] SET [Example] = @Example, [Name] = @Name WHERE [Id] = @Id">
                <DeleteParameters>
                    <asp:Parameter Name="Id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Example" Type="String" />
                    <asp:Parameter Name="Name" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Example" Type="String" />
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </h1>

        <h1>Documentation</h1>
        <p class="demo-detail">
            To connect the database, first we need to tunnel the port to login.ccs.neu.edu. We do this by adding an entry in C2S with List/Dest port 1433 and Destination Host MSSQL2005.development.ccs.neu.edu.
        </p>
        <p class="demo-detail">
            Then we go to the server explorer, right click on Data Connections, and click on Add Connection...
        </p>
        <p class="demo-detail">
            The server name should be 127.0.0.1 because we tunnal the port 1433 on the local host to the SQL server.
        </p>
        <p class="demo-detail">
            Use SQL authentication and enter the user name and password.
        </p>
        <p class="demo-detail">
            I was stuck on the error "A connection was successfully established with the server, but then an error occurred during the login process. (provider: SSL Provider, error: 0 - The message received was unexpected or badly formatted.)"
        </p>
        <p class="demo-detail">
            I thought it was because there were other local sql database running as services, so I uninstalled every sql server. But the error was not solved.
        </p> 
        <p class="demo-detail">
            After that I repaired and reinstalled VS, but still didn't work.
        </p> 
        <p class="demo-detail">
            Finally I found a post on <a href="http://stackoverflow.com/questions/21681762/how-do-i-fix-an-error-connecting-to-sql-server-ssl-provider-error-0-the-mes">StackOverflow</a>. And updated the .NET framework to solve this problem. Wasted my whole day.
            <br />
            I hate Microsoft TAT....
            <br />
            I hate Microsoft TAT....
            <br />
            I said it twice because it is important.
        </p> 
        <p class="demo-detail">
            Once I connected to the database, now it's time to add some table. I went to Server Explorer and tried to add some table. But again some error popped up and said that the sql script failed. I managed to do it in SQL Server  Object Explorer. Why?
            <br />
            Maybe it's because I didn't select a database? I right clicked on the Data Connections and clicked Modify Connection..., and I selected a database name. Now everything worked fine.
        </p>
        <p class="demo-detail">
            I added some stuff in the database, and dragged it to aspx. It worked locally. I'll see if it works on IIS, probably won't because of the connection string issue.
        </p>
        <p class="demo-detail">
            Yeah I knew it would fail, and it is caused by connection string as I would expect. I didn't fix it before I uploaded it because I revere the power of Murphy's law.
        </p>    
        <p class="demo-detail">
            Actually I did follow the <a href="http://www.ccs.neu.edu/teaching/web/connection_strings.htm" target="_blank">step</a> here. But there are some place that need to be change.
            I copied and replaced the connection string file, but the Server Explore told me connection can not be established.
            <br />
            And at that moment I figured out the ultimate answer to Web.config, connectionString.config, universe, life and everything.
            <br />
            Then I copied the ConnectionString part from web.config to connectionStrings.config, and made a copy for remote and yet another copy for server.
            <br />
            Now the connection can be established. And I dragged the table to this aspx file again.
            Everything works just fine now.
            <br />
            So Long, and Thanks for All the Fish.
        </p>    
        <h1 class="demo-detail">Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/01-databaseConnection.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/01-databaseConnection.aspx.cs" target="_blank">cs file</a></li>
            
        </ul>
    </div>
    </form>
</body>
</html>
