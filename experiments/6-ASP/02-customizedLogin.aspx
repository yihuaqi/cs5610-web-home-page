﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="02-customizedLogin.aspx.cs" Inherits="experiments_6_ASP_02_customizedLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customized Login</title>
    <link rel="stylesheet" href="../../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/experiments.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>Customized Login</h1>
        <asp:TextBox ID="usernameTxtbx" runat="server" placeholder="UserName" CssClass="form-control"></asp:TextBox>
        <asp:TextBox ID="passwordTxtbx" runat="server" placeholder="Password" CssClass="form-control"></asp:TextBox>
        <asp:Button ID="loginBtn" runat="server" Text="Login" CssClass="btn btn-block btn-success" OnClick="loginBtn_Click" />
        <asp:Button ID="logoutBtn" runat="server" Text="Logout" CssClass="btn btn-block btn-danger" OnClick="logoutBtn_Click" />
        <asp:Label ID="statusLable" runat="server" Text="Username:Hello     Password:World" CssClass="label-danger"></asp:Label>
        <br />
        <p class="demo-detail">This <a href="03-restrictedPage.aspx">page</a> is only accessable when you are logged in</p>

        <br />
        <h1>Documentation</h1>
        <p class="demo-detail">
             We can achieve customized login without using the login widget.
            <br />
            We only need to figure out a way to pass the username and password input from user, and compare them with the pair in our database.
            <br />
            This can be achieved so much easier using .Net than using Node.js. In Node.js we will need to pass it using query or post it in the body. 
            <br />
            While in .Net, we just need to drag the TextBox widget and we can reference it in CS file without any extra steps.
            <pre>
                &lt;asp:TextBox ID=&quot;TextBox1&quot; runat=&quot;server&quot; placeholder=&quot;UserName&quot; CssClass=&quot;form-control&quot;&gt;&lt;/asp:TextBox&gt;
                &lt;asp:TextBox ID=&quot;TextBox2&quot; runat=&quot;server&quot; placeholder=&quot;Password&quot; CssClass=&quot;form-control&quot;&gt;&lt;/asp:TextBox&gt;
            </pre>
            <pre>
                string username = usernameTxtbx.Text;
                string password = passwordTxtbx.Text;
            </pre>
            <br />
            Then we can drag two buttons that handle login and logout seperately.
            <br />
            The eventListener is automatically generated, which is again super nice.
            <br />
            In the login event, here I just simply compare the username and password. In final project it should be using database.
            <br />
            To mark the user as loggin, we need to store the user information into Session. Here we simply store the username in it.
            <pre>
                if (username == &quot;Hello&quot; &amp;&amp; password == &quot;World&quot;)
                {
                    statusLable.Text = &quot;You are logged In!&quot;;
                    Session[&quot;loggedIn&quot;] = username;
                }
            </pre>
            <br />
            To logout, we can set the Session to null
            <pre>
                Session[&quot;loggedIn&quot;] = null;
                statusLable.Text = &quot;You are logged out&quot;;
            </pre>
        </p>
        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/02-customizedLogin.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/02-customizedLogin.aspx.cs" target="_blank">cs file</a></li>
        </ul>
    </div>
        
    </form>
</body>
</html>
