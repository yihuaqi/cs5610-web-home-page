﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="03-restrictedPage.aspx.cs" Inherits="experiments_6_ASP_main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Restricted Page</title>
    <link rel="stylesheet" href="../../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/experiments.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div >
        <h1 class="demo-detail">
            Restricted Page
        </h1>
        <p>
            This page is restricted! You can see this only if you are logged in!
        </p>
        <p class="demo-detail">
            To forbid a, unauthorized user from accessing, one simple piece of code can do it.
            <pre>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[&quot;loggedIn&quot;] == null)
        {
            Response.Redirect(&quot;02-customizedLogin.aspx&quot;);
        }
    }
            </pre>
            This will redirect any user that has not logged in to the login in page.
        </p>
        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/03-restrictedPage.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/03-restrictedPage.aspx.cs" target="_blank">cs file</a></li>
        </ul>
    </div>
    </form>
</body>
</html>
