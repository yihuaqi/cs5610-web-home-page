﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
public partial class experiments_6_ASP_05_accessDatabase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string example = "Example " + DateTime.Now.Second.ToString();
        
        string name = "Name " + DateTime.Now.Second.ToString();
        string sql = "INSERT INTO [Example]  (example, name) VALUES (@example,@name) ";
        string connectionString = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        ConnectionString.Text = connectionString;

        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        SqlCommand command = new SqlCommand(sql, connection);
        SqlParameter exampleParam = new SqlParameter("@example", example);
        SqlParameter nameParam = new SqlParameter("@name", name);
        command.Parameters.Add(exampleParam);
        command.Parameters.Add(nameParam);
        command.ExecuteNonQuery();

        connection.Close();


    }
}