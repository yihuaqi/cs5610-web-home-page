﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="06-accessDatabaseByAjax.aspx.cs" Inherits="experiments_6_ASP_06_accessDatabaseByAjax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../../js/jquery-1.11.1.js"></script>
    <link href="../../css/bootstrap.css" rel="stylesheet"/>
    <script src="js/06-accessDatabaseByAjax.js"></script>
    <link href="../css/experiments.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>
            Access Database By Ajax
        </h1>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="Example" HeaderText="Example" SortExpression="Example" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Example] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Example] ([Example], [Name]) VALUES (@Example, @Name)" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Id], [Example], [Name] FROM [Example]" UpdateCommand="UPDATE [Example] SET [Example] = @Example, [Name] = @Name WHERE [Id] = @Id">
            <DeleteParameters>
                <asp:Parameter Name="Id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Example" Type="String" />
                <asp:Parameter Name="Name" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Example" Type="String" />
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        
        <div>
            <ul class="DBList">
                
            </ul>
        </div>
        <button class="btn btn-success getDatabtn">
            Get Data
        </button>
        <h1>Documentation</h1>
        <p class="demo-detail">
            This experiment demos a way to get data from server using Ajax.
            <br />
            Ajax stands for "Asynchronous JavaScript and XML".
            <br />
            Here we use Ajax to retrieve the content in the database.
            First we need to set up WebMethod in server side.
            <pre>
    [System.Web.Services.WebMethod]
    public static Example[] getExamples()
    {
        
        List&lt;Example&gt; examples = new List&lt;Example&gt;();
        
        string sql = &quot;SELECT * FROM [Example]&quot;;
        System.Configuration.Configuration rootWebConfig =
                System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(&quot;/yihuaqi&quot;);
        System.Configuration.ConnectionStringSettings connString;
        if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count &gt; 0)
        {
            connString =
                rootWebConfig.ConnectionStrings.ConnectionStrings[&quot;ConnectionString&quot;];
            if (connString != null){

                SqlConnection connection = new SqlConnection(connString.ConnectionString);
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Example example = new Example();
                    example.Id = reader.GetInt32(0);
                    example.ExampleId = reader.GetString(1);
                    example.Name = reader.GetString(2);
                    examples.Add(example);
                }

                connection.Close();
            }
            else
                Console.WriteLine(&quot;No connection string&quot;);
        }
        return examples.ToArray();
    }
            </pre>
            This piece of code will get all the entries in the table, and return to client as a json object.
            <br />
            Then we can fire ajax using jQuery and get the data we want.
            <pre>
        $.ajax({
            type: &quot;POST&quot;,
            url: &quot;/experiments/6-ASP/06-accessDatabaseByAjax.aspx/getExamples&quot;,
            contentType: &quot;application/json;charset=utf-8&quot;,
            dataType: &quot;json&quot;,
            success: function (data) {
                console.log(data);
                for (var i in data.d) {
                    var li = &quot;&lt;li&gt;&quot; + data.d[i].Id + &quot; &quot; + data.d[i].ExampleId + &quot; &quot; + data.d[i].Name + &quot;&lt;/li&gt;&quot;;
                    $(&quot;.DBList&quot;).append(li);
                }
            },
            failure: function (response) {
                console.log(&quot;Failure&quot;);
            }
        });
            </pre>
            One problem I found is that the ajax cannot be firely in the click event.
            It can only work outside of the click event, otherwise the whole page reloads and no data is retrieved.
            <br />
            I think the reason why the page reloads when a button is clicked is that the button is
            in the "form" tag, and this will cause the default behavior of the button to be reloading the page.
            <br />
            This piece of code solves the problem perfectly
            <pre >
                $(".getDatabtn").click(function (event) {
                    event.preventDefault();
                    ...
                });
            </pre>
            Or I can put the button outside the "form" tag to solve it.


        </p>
        <h1>Reference</h1>
        <ul>
            
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/06-accessDatabaseByAjax.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/06-accessDatabaseByAjax.aspx.cs" target="_blank">cs file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/js/06-accessDatabaseByAjax.js" target="_blank">js file</a></li>
            <li><a href="http://dotnetmentors.com/aspnet/jquery-ajax-with-pagemethod-in-asp-net.aspx" target="_blank">Using Ajax</a></li>
        </ul>
    </div>
    </form>
</body>
</html>
