﻿$(document).ready(function () {
    $(".getDatabtn").click(function (event) {
        event.preventDefault();
        console.log("clicked");
        $.ajax({
            type: "POST",
            url: "06-accessDatabaseByAjax.aspx/getExamples",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data);
                for (var i in data.d) {
                    var li = "<li>" + data.d[i].Id + " " + data.d[i].ExampleId + " " + data.d[i].Name + "</li>";
                    $(".DBList").append(li);
                }
            },
            failure: function (response) {
                console.log("Failure");
            }
        });
    });



});