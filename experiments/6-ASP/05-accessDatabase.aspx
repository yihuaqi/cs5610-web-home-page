﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="05-accessDatabase.aspx.cs" Inherits="experiments_6_ASP_05_accessDatabase" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../../js/jquery-1.11.1.js"></script>


    <link href="../../css/bootstrap.css" rel="stylesheet" />
    <link href="../css/experiments.css" rel="stylesheet" />

    <title></title>
</head>
<body>
    

    <form id="form1" runat="server">
        <div class="demo-detail">
           <h1>
               Access Database
           </h1>
            <div >
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                        <asp:BoundField DataField="Example" HeaderText="Example" SortExpression="Example" />
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Example] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Example] ([Example], [Name]) VALUES (@Example, @Name)" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Id], [Example], [Name] FROM [Example]" UpdateCommand="UPDATE [Example] SET [Example] = @Example, [Name] = @Name WHERE [Id] = @Id">
                    <DeleteParameters>
                        <asp:Parameter Name="Id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Example" Type="String" />
                        <asp:Parameter Name="Name" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Example" Type="String" />
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>

                <asp:Button ID="Button1" runat="server" Text="Add Something to database" OnClick="Button1_Click"/>
                <br />
                <asp:Literal ID="ConnectionString" runat="server"></asp:Literal>
            </div>
            
            <h1>Documentation</h1>
            <p class="demo-detail">
                Here I tried to do a insertion into database in C#.
                <pre>
        string example = &quot;Example &quot; + DateTime.Now.Second.ToString();
        
        string name = &quot;Name &quot; + DateTime.Now.Second.ToString();
        string sql = &quot;INSERT INTO [Example]  (example, name) VALUES (@example,@name) &quot;;
        string connectionString = WebConfigurationManager.ConnectionStrings[&quot;ConnectionString&quot;].ConnectionString;

        ConnectionString.Text = connectionString;

        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        SqlCommand command = new SqlCommand(sql, connection);
        SqlParameter exampleParam = new SqlParameter(&quot;@example&quot;, example);
        SqlParameter nameParam = new SqlParameter(&quot;@name&quot;, name);
        command.Parameters.Add(exampleParam);
        command.Parameters.Add(nameParam);
        command.ExecuteNonQuery();

        connection.Close();
                </pre>
                First we need to read the connection string. We can't hardcode the connection string because it changes in different environments.
                Here is how we read in the web.config by the official guide.
                ....But then I found this only works on my computer.
                <pre>
        System.Configuration.Configuration rootWebConfig =
                System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(&quot;/yihuaqi&quot;);
        System.Configuration.ConnectionStringSettings connString;
        if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count &gt; 0)
        {
            connString =
                rootWebConfig.ConnectionStrings.ConnectionStrings[&quot;ConnectionString&quot;];
            ...
        }
                </pre>
                The correct way to do this is by:
                <pre>
        string connectionString = WebConfigurationManager.ConnectionStrings[&quot;ConnectionString&quot;].ConnectionString;

        ConnectionString.Text = connectionString;

        SqlConnection connection = new SqlConnection(connectionString);
                </pre>
                Then we just open the connection using the connection string, and don't forget to close it.
                Everything is the same as in the class on 11/10/2014.

            </p>


            <h1>Reference</h1>
            <ul>
                <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/05-accessDatabase.aspx" target="_blank">aspx file</a></li>
                <li><a href="../../fileview/Default.aspx?~/experiments/6-ASP/05-accessDatabase.aspx.cs" target="_blank">cs file</a></li>
                <li><a href="http://msdn.microsoft.com/en-us/library/ms178411%28v=vs.100%29.aspx" target="_blank">Get ConnectionString From Web.config</a></li>
            </ul>

        </div>
    </form>

</body>
</html>
