var http = require("http")
var express = require('express');
var app = express();
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var address = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var buttonClickCount = 0;

app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.get('/buttonClick',function(req,res){
	buttonClickCount = buttonClickCount + 1;
	res.send(buttonClickCount.toString());
	console.log(buttonClickCount.toString());


});


app.get('/hello.txt', function(req, res){
  res.send('Hello World！！！');
});

var book1 = {
    isbn: "1234",
    title: "Book 1",
    author: "Author 1",
};
var book2 = {
    isbn: "2234",
    title: "Book 2",
    author: "Author 2",
};
var book3 = {
    isbn: "3234",
    title: "Book 3",
    author: "Author 3",
};
var books = [book1, book2, book3];

app.get('/getAllBooks',function(req,res){
	res.json(books);

});

app.get('/addBook',function(req,res){
	var newIsbn = req.query.isbn;
	var newTitle = req.query.title;
	var newAuthor = req.query.author;
	var duplicate = false;
	for (var i = books.length - 1; i >= 0; i--) {
		if(books[i].isbn == newIsbn){
			duplicate = true;
		}
	};
	if(duplicate){
		res.send("Duplicate ISBN!");
	} else {
		var newbook = {
			isbn: newIsbn,
			title: newTitle,
			author: newAuthor
		};
		books.push(newbook);
		res.send("Add Success!");
		
	}
});

app.get('/deleteBook',function(req,res){
	var deleteISBN = req.query.isbn;
	var result = false;
	for (var i = books.length - 1; i >= 0; i--) {
		if(books[i].isbn == deleteISBN){
			result = true;
			books.splice(i,1);
		}
	};
	if(result){
		res.send("Delete Success!");
	} else {
		res.send("ISBN does not exist!");
	}
});

var server = app.listen(port,address, function() {
    console.log('Listening on port %d', server.address().port);
});
