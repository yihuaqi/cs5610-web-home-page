﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="02-ajaxWithJson.aspx.cs" Inherits="experiments_8_Ajax_02_ajaxWithJson" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../../js/jquery-1.11.1.js"></script>
    <link href="../../css/bootstrap.css" rel="stylesheet"/>
    <script src="js/02-ajaxWithJson.js"></script>
    <link href="../css/experiments.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
                <asp:BoundField DataField="type" HeaderText="type" SortExpression="type" />
                <asp:BoundField DataField="description" HeaderText="description" SortExpression="description" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Anime] WHERE [Id] = @Id" InsertCommand="INSERT INTO [Anime] ([name], [type], [description]) VALUES (@name, @type, @description)" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Id], [name], [type], [description] FROM [Anime]" UpdateCommand="UPDATE [Anime] SET [name] = @name, [type] = @type, [description] = @description WHERE [Id] = @Id">
            <DeleteParameters>
                <asp:Parameter Name="Id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="type" Type="String" />
                <asp:Parameter Name="description" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="type" Type="String" />
                <asp:Parameter Name="description" Type="String" />
                <asp:Parameter Name="Id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>
    </form>
    <div class="container">
        <button id="helloBtn" class="btn btn-success">Hello</button>
        <input id="Id" class="form-control" placeholder="Enter the id"/>
        <p id="text">Hello</p>
        <h1>Ajax with Json</h1>
        <p class="demo-detail">
            This time I will try to return an json from the server and parse it in javascript.
            <br/>
            On the server side:
            <pre>
    [System.Web.Services.WebMethod]
    public static string hello()
    {
        string Id = HttpContext.Current.Request.Params[&quot;Id&quot;];
        int id = Convert.ToInt32(Id);
        
        using (var context = new yihuaqiEntities())
        {

            var animes = from a in context.Anime
                         where a.Id==id
                         select new { a.Id, a.name, a.type, a.description };
            JavaScriptSerializer jss = new JavaScriptSerializer();
            var json = jss.Serialize(animes);
            return json;
        }
        
    }
    </pre>
            We use entity framework and linq here to get a list of animes.
            <br/>
            The trick is that there is no json representation in C#. How do we return a json object?
            <br/>
            Here we use a JavaScriptSerializer to serialize the json object into a string, and try to parse it in the javascript.
            <br/>
            Remember to add "using System.Web.Script.Serialization;" at the start of the C#.
            <br/>
            Below is how we parse it.
            <pre>
    
    $(&quot;#helloBtn&quot;).click(function () {
        var id = $(&quot;#Id&quot;).val();
        $.ajax({
            type: &quot;POST&quot;,
            url: &quot;02-ajaxWithJson.aspx/hello?Id=&quot; + id,
            contentType: &quot;application/json;charset=utf-8&quot;,
            dataType: &quot;json&quot;,
            success: function (data) {
                console.log(data);
                var anime = JSON.parse(data.d);
                $(&quot;#text&quot;).text(&quot;Hello from &quot;+anime[0].name);
            },
            failure: function (response) {
                console.log(&quot;Failure&quot;);
            }
        });
    });
</pre>
            We call JSON.parse to the string, and it automatically returns a list of json.
            <br/>
            Notice here we are getting a list of json, instead of calling anime.name, we call anime[0].name.
            <br/>
            This is because the LINQ statement returns a list of record which accidentally will have at most one record in it (we are using primary key).
        </p>
        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/02-ajaxWithJson.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/02-ajaxWithJson.aspx.cs" target="_blank">cs file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/js/02-ajaxWithJson.js" target="_blank">js file</a></li>
        </ul>
        
    </div>
</body>
</html>
