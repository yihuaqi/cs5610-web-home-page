﻿$(function () {
    
    $("#helloBtn").click(function () {
        var id = $("#Id").val();
        $.ajax({
            type: "POST",
            url: "02-ajaxWithJson.aspx/hello?Id=" + id,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data);
                var anime = JSON.parse(data.d);
                $("#text").text("Hello from "+anime[0].name);
            },
            failure: function (response) {
                console.log("Failure");
            }
        });
    });
});