﻿$(function () {
    
    $("#helloBtn").click(function () {
        var who = $("#who").val();
        console.log("what is the value of "+who);
        $.ajax({
            type: "POST",
            url: "01-ajaxWithParams.aspx/hello?who="+who,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
                
                console.log(data);
                $("#text").text(data.d);
            },
            failure: function (response) {
                console.log("Failure");
            }
        });
    });
});