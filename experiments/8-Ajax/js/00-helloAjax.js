﻿$(function() {
    $("#helloBtn").click(function () {
        $.ajax({
            type: "POST",
            url: "00-helloAjax.aspx/hello",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data);
                $("#text").text(data.d);
            },
            failure: function (response) {
                console.log("Failure");
            }
        });
    });
});