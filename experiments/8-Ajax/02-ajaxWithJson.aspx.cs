﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
public partial class experiments_8_Ajax_02_ajaxWithJson : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod]
    public static string hello()
    {
        string Id = HttpContext.Current.Request.Params["Id"];
        int id = Convert.ToInt32(Id);
        
        using (var context = new yihuaqiEntities())
        {

            var animes = from a in context.Anime
                         where a.Id==id
                         select new { a.Id, a.name, a.type, a.description };
            JavaScriptSerializer jss = new JavaScriptSerializer();
            var json = jss.Serialize(animes);
            return json;
        }
        
    }
}