﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class experiments_8_Ajax_01_ajaxWithParams : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod]
    public static string hello()
    {
        string who = HttpContext.Current.Request.Params["who"];
        return "hello from "+who+"!!!";
    }
}