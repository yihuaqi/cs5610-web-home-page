﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="01-ajaxWithParams.aspx.cs" Inherits="experiments_8_Ajax_01_ajaxWithParams" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../../js/jquery-1.11.1.js"></script>
    <link href="../../css/bootstrap.css" rel="stylesheet"/>
    <script src="js/01-ajaxWithParams.js"></script>
    <link href="../css/experiments.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
    <div class="container">
        <button id="helloBtn" class="btn btn-success">Hello</button>
        <input id="who" class="form-control" placeholder="from who?"/>
        <p id="text">Hello</p>
        <h1>Ajax with Params</h1>
        <p class="demo-detail">This time I will try to put some parameters using ajax</p>
        <pre>
    
    $(&quot;#helloBtn&quot;).click(function () {
        var who = $(&quot;#who&quot;).val();
        $.ajax({
            type: &quot;POST&quot;,
            url: &quot;01-ajaxWithParams.aspx/hello?who=&quot;+who,
            contentType: &quot;application/json;charset=utf-8&quot;,
            dataType: &quot;json&quot;,
            success: function (data) {
                console.log(data);
                $(&quot;#text&quot;).text(data.d);
            },
            failure: function (response) {
                console.log(&quot;Failure&quot;);
            }
        });
    });
</pre>
        Here we put a parameter on the url. How do we get it?
        <br/>
        <pre>
    [System.Web.Services.WebMethod]
    public static string hello()
    {
        string who = HttpContext.Current.Request.Params[&quot;who&quot;];
        return &quot;hello from &quot;+who+&quot;!!!&quot;;
    }
</pre>
        We can directly call Request.Params in the Page_Load method, because we already have the context.
        <br/>
        In a static method we can't. Thus we need to manually get the context by calling HttpContext.Current.Request.Params.
        <br/>
        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/01-ajaxWithParams.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/01-ajaxWithParams.aspx.cs" target="_blank">cs file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/js/01-ajaxWithParams.js" target="_blank">js file</a></li>
        </ul>
    </div>
</body>
</html>
