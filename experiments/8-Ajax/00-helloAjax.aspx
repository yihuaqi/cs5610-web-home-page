﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="00-helloAjax.aspx.cs" Inherits="experiments_8_Ajax_00_helloAjax" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../../js/jquery-1.11.1.js"></script>
    <link href="../../css/bootstrap.css" rel="stylesheet"/>
    <script src="js/00-helloAjax.js"></script>
    <link href="../css/experiments.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
    </div>
    </form>
    <div class="container">
        <button id="helloBtn" class="btn btn-success">Hello</button>
        <p id="text">Hello</p>
        <h1>Hello Ajax</h1>
        <p class="demo-detail">
            Here we use ajax to get a "hello" string from the server.
        </p>
        <pre>
        $.ajax({
            type: &quot;POST&quot;,
            url: &quot;00-helloAjax.aspx/hello&quot;,
            contentType: &quot;application/json;charset=utf-8&quot;,
            dataType: &quot;json&quot;,
            success: function (data) {
                console.log(data);
                $(&quot;#text&quot;).text(data.d);
            },
            failure: function (response) {
                console.log(&quot;Failure&quot;);
            }
        });
</pre>
        We can set type to be get or post or other methods. The url is set on the server for us to hit.
        <br/>
        In theory we can post json to the server but i cannot make it work.
        <br/>
        On success event we can process the response and do whatever we want.
        <br/>
        All the data are in data.d.
        <br />
        Below is the server side code:
        <pre>
    [System.Web.Services.WebMethod]
    public static string hello()
    {
        return &quot;hello from server!&quot;;
    }
</pre>
        The method must be static, otherwise there will be no response.
        <br/>
        We must add "[System.Web.Services.WebMethod]", otherwise it won't response.
        <br/>
        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/00-helloAjax.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/00-helloAjax.aspx.cs" target="_blank">cs file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/8-Ajax/js/00-helloAjax.js" target="_blank">js file</a></li>
        </ul>
    </div>
</body>
</html>
