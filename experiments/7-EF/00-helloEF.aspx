﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="00-helloEF.aspx.cs" Inherits="experiments_7_EF_00_helloEF" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hello EF</title>
    <link rel="stylesheet" href="../../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/experiments.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>Hello EF</h1>
        <p class="demo-detail">
            Entity framework is a very easy-to-use framework that makes the interaction with database much easier.
            It gives you free api for select, update, create and delete records in the database.
            It provides na object-orientated data mode, which makes selecting using foreign keys much easier.
        </p>
        <h1>Documentation</h1>
        <p class="demo-detail">
            To set up the entity framework, we right click on the root of project, Add->ADO.NET Entity Data Model. Then we give it a name.
            <br/>
            Then we choose EF Designer from database, because we want to derive a schema from an existing database.
            <br/>
            Then we choose the database we want to connect to.
            <br/>
            Also a connection string will be set and we will need to manipulate it to make it work both locally and remotely.
            <br/>
            After that we can enjoy the benefits introduced by entity framework.

        </p>
        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/7-EF/00-helloEF.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/7-EF/00-helloEF.aspx.cs" target="_blank">cs file</a></li>
        </ul>
    </div>
    </form>
</body>
</html>
