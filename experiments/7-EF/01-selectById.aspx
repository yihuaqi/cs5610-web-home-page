﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="01-selectById.aspx.cs" Inherits="experiments_7_EF_01_selectById" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="../../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/experiments.css" /> 
    <title>Select EF</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>Select in Entity Framework</h1>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="age" HeaderText="age" SortExpression="age" />
                <asp:BoundField DataField="gender" HeaderText="gender" SortExpression="gender" />
                <asp:BoundField DataField="description" HeaderText="description" SortExpression="description" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [CV] WHERE [Id] = @Id" InsertCommand="INSERT INTO [CV] ([Name], [age], [gender], [description]) VALUES (@Name, @age, @gender, @description)" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Id], [Name], [age], [gender], [description] FROM [CV]" UpdateCommand="UPDATE [CV] SET [Name] = @Name, [age] = @age, [gender] = @gender, [description] = @description WHERE [Id] = @Id">
            <DeleteParameters>
                <asp:Parameter Name="Id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="age" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="description" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="age" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="description" Type="String" />
                <asp:Parameter Name="Id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:TextBox runat="server" placeholder="Enter the id of cv" ID="searchId"></asp:TextBox>
        <asp:Button runat="server" CssClass="btn btn-success" Text="Search" ID="search" OnClick="search_Click"/>
        <asp:Literal runat="server" ID="name"></asp:Literal>
        <asp:Literal runat="server" ID="age"></asp:Literal>
        <asp:Literal runat="server" ID="gender"></asp:Literal>
        <asp:Literal runat="server" ID="description"></asp:Literal>
        
        <h1>Documentation</h1>
        <p class="demo-detail">
            This experiment shows how to select a record by ID using Entity Framework.
            <br/>
            <pre>
        using (var context = new yihuaqiEntities())
        {
            int id = Convert.ToInt32(searchId.Text);
            CV cv = context.CV.SingleOrDefault(CV =&gt; CV.Id == id);
            if (cv != null)
            {

                name.Text = cv.Name;
                age.Text = cv.age;
                gender.Text = cv.gender;
                description.Text = cv.description;
            }
            else
            {
                name.Text = &quot;Cannot find the CV!&quot;;
                age.Text = &quot;&quot;;
                gender.Text = &quot;&quot;;
                description.Text = &quot;&quot;;
            }
        }
            </pre>
             using (var context = new yihuaqiEntities()) is creating a context for the entities object.
            <br/>
            Then we can select the record by id.
            <pre>
                CV cv = context.CV.SingleOrDefault(CV =&gt; CV.Id == id);
            </pre>
            The SingleOrDefault function will return one record if there is one. If there are more than one records, it will throw an exception. If no record is found, then it will return null.
            <br/>
            Super Easy to use.
        </p>
        <h1>Reference</h1>
        <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/7-EF/01-selectById.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/7-EF/01-selectById.aspx.cs" target="_blank">cs file</a></li>
        </ul>
    </div>
    </form>
</body>
</html>
