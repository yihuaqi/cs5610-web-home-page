﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class experiments_7_EF_01_selecteByName : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void search_Click(object sender, EventArgs e)
    {
            
        using (var context = new yihuaqiEntities())
        {
            string name = searchName.Text;
            var cvs = from c in context.CV
                where c.Name.Contains(name)
                select c;
            
            if (cvs != null)
            {
                number.Text = cvs.Count().ToString();
            }
        }
    }
}