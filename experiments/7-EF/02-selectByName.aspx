﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="02-selectByName.aspx.cs" Inherits="experiments_7_EF_01_selecteByName" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/experiments.css" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>Select in Entity Framework</h1>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="age" HeaderText="age" SortExpression="age" />
                <asp:BoundField DataField="gender" HeaderText="gender" SortExpression="gender" />
                <asp:BoundField DataField="description" HeaderText="description" SortExpression="description" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [CV] WHERE [Id] = @Id" InsertCommand="INSERT INTO [CV] ([Name], [age], [gender], [description]) VALUES (@Name, @age, @gender, @description)" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Id], [Name], [age], [gender], [description] FROM [CV]" UpdateCommand="UPDATE [CV] SET [Name] = @Name, [age] = @age, [gender] = @gender, [description] = @description WHERE [Id] = @Id">
            <DeleteParameters>
                <asp:Parameter Name="Id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="age" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="description" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="age" Type="String" />
                <asp:Parameter Name="gender" Type="String" />
                <asp:Parameter Name="description" Type="String" />
                <asp:Parameter Name="Id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:TextBox runat="server" placeholder="Enter part of the name of cv" ID="searchName"></asp:TextBox>
        <asp:Button runat="server" CssClass="btn btn-success" Text="Search" ID="search" OnClick="search_Click"/>
        The number of cv whose name contains the given string is <asp:Literal runat="server" ID="number"></asp:Literal>
        
        
        <h1>Documentation</h1>
        <p class="demo-detail">
            This experiment shows how to select all the records by part of its name using Entity Framework.
            <br/>
            <pre>
        using (var context = new yihuaqiEntities())
        {
            string name = searchName.Text;
            var cvs = from c in context.CV
                where c.Name.Contains(name)
                select c;
            
            if (cvs != null)
            {
                number.Text = cvs.Count().ToString();
            }
        }
            </pre>
            The "from...in..where...select" is a LINQ statement, which is a very powerful tool to select things from the entity framework.

            <br/>
            By using the LINQ statement we can have an array of the object.
            Here we only display the count of the array.
            <pre>
                number.Text = cvs.Count().ToString();
            </pre>
            
            <br/>
            Again super Easy to use.
        </p>
        <h1>Reference</h1>
       <ul>
            <li><a href="../../fileview/Default.aspx?~/experiments/7-EF/02-selectByName.aspx" target="_blank">aspx file</a></li>
            <li><a href="../../fileview/Default.aspx?~/experiments/7-EF/02-selectByName.aspx.cs" target="_blank">cs file</a></li>
           <li><a href="https://code.msdn.microsoft.com/101-linq-samples-3fb9811b" target="_blank">LINQ Example</a></li>
        </ul>
        
    </div>
    </form>
</body>
</html>
