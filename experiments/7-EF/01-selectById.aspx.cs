﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class experiments_7_EF_01_selectById : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void search_Click(object sender, EventArgs e)
    {
        using (var context = new yihuaqiEntities())
        {
            try
            {

                int id = Convert.ToInt32(searchId.Text);
                CV cv = context.CV.SingleOrDefault(CV => CV.Id == id);
                if (cv != null)
                {

                    name.Text = cv.Name;
                    age.Text = cv.age;
                    gender.Text = cv.gender;
                    description.Text = cv.description;
                }
                else
                {
                    name.Text = "Cannot find the CV!";
                    age.Text = "";
                    gender.Text = "";
                    description.Text = "";
                }
            }
            catch (Exception )
            {
                name.Text = "Please enter a number";
                age.Text = "";
                gender.Text = "";
                description.Text = "";
                
            }
        }
    }
}