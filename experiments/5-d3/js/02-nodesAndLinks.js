﻿var nodes = [];
var links = [];
$(function () {
    var width = 960;
    var height = 500;
    
    d3.select("svg").on("mousedown", mousedown);
    
   
});

function mousedown () {
    var point = d3.mouse(this);
    
    var node = { x: point[0], y: point[1] };
    nodes.push(node);

    
    console.log(nodes.length);
    nodes.forEach(function (target) {
        links.push({ source: node, target: target });
    });
    
    d3.select("svg")
    .selectAll("circle")
    .data(nodes)
    .enter().append("circle")
    .attr("cx", function (d) { console.log(d);return d.x; })
    .attr("cy", function (d) { return d.y; })
    .attr("r", 15);

    d3.select("svg")
    .selectAll(".line")
    .data(links)
    .enter().append("line")
    .attr("x1", function (d) { return d.source.x; })
    .attr("y1", function (d) { return d.source.y; })
    .attr("x2", function (d) { return d.target.x; })
    .attr("y2", function (d) { return d.target.y; })
    .style("stroke", "black");
}