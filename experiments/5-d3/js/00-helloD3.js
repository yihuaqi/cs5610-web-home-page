﻿$(function () {
    var largeCircle = d3.selectAll("circle.large");
    largeCircle.style("fill", "steelblue");
    largeCircle.attr("r", 30);

    var dancingCircle = d3.selectAll("circle.dancing");
    $("circle.dancing").mouseenter(function () {
        dancingCircle.attr("cx", function () { return Math.random() * 720; });
    });

    var dataToCircle = d3.selectAll("circle.dataToCircle");
    var data = [32, 57, 112];
    dataToCircle.data(data);
    dataToCircle.attr("r", function (d) { return Math.sqrt(d); });
    dataToCircle.attr("cx", function (d, i) { return i * 100 + 30; });


    //$("#addNewCircle").click(function () {
        
    //    var radius = $("input[name=addNewCircleItem]").val();
    //    data.push(radius);
    //    for(var i = 0; i < data.length;i++){
    //        console.log(data[i]);
    //    }
        
    //    //d3.selectAll("circle.dataToCircle")
    //    //    .data(data)
    //    //    .enter().append("circle.dataToCircle")
    //    //    .attr("cy", 60)
    //    //    .attr("cx", function (d, i) { return i * 100 + 30; })
    //    //    .attr("r", function (d) { return Math.sqrt(d); });
    //    var svg = d3.select("svg");

    //    var circle = svg.selectAll("circle")
    //        .data([32, 57, 112, 293]);

    //    var circleEnter = circle.enter().append("circle");
    //    circleEnter.attr("cy", 60);
    //    circleEnter.attr("cx", function (d, i) { return i * 100 + 30; });
    //    circleEnter.attr("r", function (d) { return Math.sqrt(d); });
    //});
});