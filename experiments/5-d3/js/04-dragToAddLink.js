﻿$(function () {

    var width = 960,
        height = 500,
        nodeRadius = 15;

    var mousedown_node = null,
        mouseup_node = null; 
        

    function resetMouseVars() {
        
        mousedown_node = null;
        mouseup_node = null;
    
    
        
    }
    //var fill = d3.scale.category20();

    var force = d3.layout.force()
        .size([width, height])
        .nodes([{}]) // initialize with a single node
        .linkDistance(150)
        .charge(-500)
        .on("tick", tick);

    var svg = d3.select("div :first-child").append("svg")
        .attr("width", width)
        .attr("height", height)
        .on("mousemove", mousemove)
        .on("mouseup",mouseup)
        .on("mousedown", mousedown);

    svg.append("rect")
        .attr("width", width)
        .attr("height", height)
        .style("stroke", "black");

    // line displayed when dragging new nodes
    var drag_line = svg.append('svg:path')
      .attr('class', 'link dragline hidden')
      .attr('d', 'M0,0L0,0');

    var nodes = force.nodes(),
        links = force.links(),
        node = svg.selectAll(".node"),
        link = svg.selectAll(".link");

    var cursor = svg.append("circle")
        .attr("transform", "translate(-100,-100)")
        .attr("class", "cursor");

    restart();

    function mousemove() {
        if (!mousedown_node) return;
        drag_line.attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + d3.mouse(this)[0] + ',' + d3.mouse(this)[1]);
    }

    function mousedown() {
        
        if (mousedown_node) return;
        var point = d3.mouse(this),
            node = { x: point[0], y: point[1] },
            n = nodes.push(node);

        restart();
    }

    function mouseup() {
        if (mousedown_node) {
            drag_line
             .classed('hidden', true);
        }
        resetMouseVars();
        
    }

    function tick() {
        link.attr("x1", function (d) { return d.source.x; })
            .attr("y1", function (d) { return d.source.y; })
            .attr("x2", function (d) { return d.target.x; })
            .attr("y2", function (d) { return d.target.y; });

        node.attr("cx", function (d) { return d.x; })
            .attr("cy", function (d) { return d.y; });
    }

    function restart() {
        link = link.data(links);

        link.enter().insert("line", ".node")
            .attr("class", "link");

        node = node.data(nodes);

        node.enter().insert("circle", ".cursor")
            .attr("class", "node")
            .attr("r", nodeRadius)
            .on('mousedown', function (d) {
                mousedown_node = d;
                // reposition drag line
                drag_line
                  .style('marker-end', 'url(#end-arrow)')
                  .classed('hidden', false)
                  .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);
                restart();
            })
            .on('mouseup', function (d) {
                if (!mousedown_node) return;
                drag_line
                .classed('hidden', true)
                .style('marker-end', '');
                mouseup_node = d;
                if (mouseup_node == mousedown_node) {
                    resetMouseVars();
                    return;
                }
                // Create a link beteen two nodes
                var source, target;
                source = mousedown_node;
                target = mouseup_node;
                var link;
                link = links.filter(function (l) {
                    return ((l.source == source && l.target == target) || (l.target == source && l.source == target));
                })[0];
                if(link){
                    // Link already exists.
                    return;
                } else {
                    link = {source:source,target:target};
                    links.push(link);
                }
                resetMouseVars();
                restart();
            });
            

        force.start();
    }

});