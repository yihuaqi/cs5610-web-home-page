﻿$(function () {
    var data = []

    $("button#addCircle").click(function () {
        console.log("click");
        data.push($("input[name=circleRadius]").val());
        d3.select("svg")
            .selectAll("circle")
            .data(data)
            .enter().append("circle")
            .attr("cy", 60)
            .attr("cx", function (d, i) { return i * 50 + 30; })
            .attr("r", function (d) { return d; });
    }); 

    $("button#deleteCircle").click(function () {
        console.log("click");
        data.splice(0, 1);
        for (var i = 0; i < data.length; i++) {
            console.log(data[i]);
        }
        d3.select("svg")
            .selectAll("circle")
            .data(data)
            .exit().remove();

        d3.select("svg")
            .selectAll("circle")
            .attr("cy", 60)
            .attr("cx", function (d, i) { return i * 50 + 30; })
            .attr("r", function (d) { return d; });

    });
});