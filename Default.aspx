﻿<%@ Page Language="C#" %>

<script runat="server">
    <%-- This demo page has no server side script --%>
</script>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset='utf-8' />

    <title>Huaqi's Home Page</title>
    <link href="images/home_page/littleBlackIcon.jpg" rel="shortcut icon"/>
    <style type="text/css">
        ul.master_navigation {
            font-size: 100%;
            font-weight: bold;
            text-align: center;
            list-style: none;
            margin: 0.5em 0;
            padding: 0;
        }

            ul.master_navigation li {
                display: inline-block;
                padding: 0 0.5%;
            }

        a {
            color: #08f;
            font-weight: bold;
            text-decoration: none;
        }

            a:visited {
                color: #88f;
            }

            a:hover {
                color: #f00;
            }

        p {
            text-align: justify;
        }
    </style>

    <style type="text/css" media="screen">
        body {
            width: 900px;
            max-width: 100%;
            margin: 0;
            padding: 0;
        }

        .pad {
            padding: 10px;
        }
    </style>

    <link href="css/homepage.css" type="text/css" rel="stylesheet" />

</head>

<body>

    <div class="pad">

        <form id="form1" runat="server">

            <div>

                <ul class="master_navigation">
                    <li><a href="sitestatistics/" target="_blank">SiteStatistics</a></li>
                    <li><a href="statistics/" target="_blank">Statistics</a></li>
                    <li><a href="source/" target="_blank">Source</a></li>
                    <li><a href="search/" target="_blank">Search</a></li>
                    <li><a href="searchtree/" target="_blank">SearchTree</a></li>
                    <li><a href="textview/" target="_blank">TextView</a></li>
                    <li><a href="filelist.aspx" target="_blank">FileList</a></li>
                    <li><a href="autofile.aspx" target="_blank">AutoFile</a></li>
                    <li><a href="images/autoimage.aspx" target="_blank">Images</a></li>
                    <li><a href="blog/" target="_blank">Blog</a></li>
                    <li><a href="story/index.htm?../experiments/" target="_blank">Experiments</a></li>

                </ul>

                <hr />
               


            </div>


            
           

            <div>
                <img class="my_image_src" src="images/home_page/yihuaqi.jpg" />
            </div>

            <div class="my_intro">
                
                <h1 class="my_intro_title">Introduction</h1>
                
                <br />
                <p class="my_intro_content">I am Huaqi, a seconde year graduate student in Computer Science at Northeastern University. I hope I can learn a lot of basic knowledge and cool stuff, and build a very cool website.</p>
                 
            </div>
            <div class="minecraft_walking_zone" >
                <!--<img src="images/home_page/Minecraft_Enemies.gif" /> -->
            </div>

        </form>


    </div>


</body>
</html>
