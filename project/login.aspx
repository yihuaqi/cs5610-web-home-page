﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <h1>Login</h1>
        <div>
            <asp:TextBox ID="usernameTxtbx" runat="server" placeholder="UserName" CssClass="form-control"></asp:TextBox>
            <asp:TextBox ID="passwordTxtbx" runat="server" placeholder="Password" CssClass="form-control"></asp:TextBox>
            <asp:Button ID="loginBtn" runat="server" Text="Login" CssClass="btn btn-block btn-success" OnClick="loginBtn_Click" />
            <asp:Button ID="logoutBtn" runat="server" Text="Logout" CssClass="btn btn-block btn-danger" OnClick="logoutBtn_Click" />
            <asp:Label ID="statusLable" runat="server" Text="Username:Hello     Password:World" CssClass="label-danger"></asp:Label>
            <ul>
                <asp:Repeater ID="userRepeater" runat="server">
                    <ItemTemplate>
                        <li>
                            <%# Eval("username") %>
                            <%# Eval("password") %>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>

            </ul>
        </div>
    </div>
    </form>
</body>
</html>
