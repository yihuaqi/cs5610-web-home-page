﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class project_createCV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int id = Convert.ToInt32(Request["cvId"]);
            using (var context = new yihuaqiEntities())
            {
                CV cv = context.CV.SingleOrDefault(CV => CV.Id == id);
                if (cv != null) {
                    cv_name_textbox.Text = cv.Name;
                    cv_age_textboxp.Text = cv.age;
                    cv_gender_textbox.Text = cv.gender;
                    cv_description_textbox.Text = cv.description;
                }
                    
            }
        }
    }
    protected void cv_submit_btn_Click(object sender, EventArgs e)
    {
        
        string imagesString = Request.Form["cv_images_input"];
        
        string[] images = imagesString.Split(new string[] { "???" }, StringSplitOptions.RemoveEmptyEntries);        
        string cvName = cv_name_textbox.Text;
        string cvAge = cv_age_textboxp.Text;
        string cvGender = cv_gender_textbox.Text;
        string cvDescription = cv_description_textbox.Text;
        using (var context = new yihuaqiEntities()) { 
            if (Request["cvId"] != null)
            {
                int id = Convert.ToInt32(Request["cvId"]);
                CV cv = context.CV.SingleOrDefault(CV => CV.Id == id);
                cv.Name = cvName;
                cv.age = cvAge;
                cv.gender = cvGender;
                cv.description = cvDescription;
                context.SaveChanges();
            }
            else
            {
                CV newCV = new CV { Name = cvName, age = cvAge, gender = cvGender, description = cvDescription };
                
                context.CV.Add(newCV);
                context.SaveChanges();   
            }
        }

    }
}