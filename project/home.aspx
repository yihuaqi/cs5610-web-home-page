﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/bootstrap.css" rel="stylesheet"/>
    <link href="css/home.css" rel="stylesheet" />
    <script src="../js/d3.js"></script>
    <script src="../js/jquery-1.11.1.js"></script>
    <script src="js/home.js"></script>
    <title></title>
</head>
<body>
    
    <div class="container">
        <h1>Home</h1>
        <div class="col-xs-6">
            <input class="form-control " id="searchName" placeholder="Name of CV/Anime/Character" />
        </div>
        <div class="col-xs-2" >
            <select class="form-control " id="searchType">
                <option value="cv">CV</option>
                <option value="character">Character</option>
                <option value="anime">Anime</option>
            </select>
        </div>
        <div class="col-xs-1">
            <button class="btn btn-success btn-block" id="searchBtn">Search</button>
        </div>
        
        <div class="col-xs-1" id="editModeDiv">
            <button id="editModeButton" class="btn-block btn btn-warning">Edit</button>
        </div>
        <div class="col-xs-1" id="viewModeDiv" style="display:none" >
            <button id="viewModeButton" class="btn-block btn btn-warning">View</button>
        </div>
        <br/>
        <br/>

        <div class="form-group-sm" id="modeDiv" style="display:none">
            
            <label class=" control-label col-xs-4" id="createTypeTextDiv"  >
                Click on blank area to create 
                
                
            </label>
            
            <div class="col-xs-2 " id="createTypeDiv" >
            
                <select class="form-control" id="createType">
                    <option value="cv">CV</option>
                    <option value="character">Character</option>
                    <option value="anime">Anime</option>
                </select>
            </div>
            
            
            
        </div>
        <br />



    </div>

                <div class="visualization">

        </div>
</body>
</html>
