﻿$(document).ready(function () {
    
    setUpBtns();
    var mode = "view";
        
    function setUpBtns() {
        $("#searchBtn").click(function(event) {
            event.preventDefault();
            

            var type = $("#searchType").val();
            var name = $("#searchName").val();
            
            
            //test();
            switch (type) {
            case "cv":
                getCVByName(name);
                break;
            case "character":
                getCharacterByName(name);
                break;
            case "anime":
                getAnimeByName(name);
                break;
            }
        });

        $("#editModeButton").click(function (event) {
            mode = "edit";
            //$("#createTypeDiv").removeClass("hidden");
            $("#viewModeDiv").show();
            //$("#createTypeTextDiv").removeClass("hidden");
            $("#editModeDiv").hide();

            $("#modeDiv").show();
            
        });

        $("#viewModeButton").click(function (event) {
            mode = "view";
            $("#editModeDiv").show();
            //$("#createTypeDiv").addClass("hidden");
            //$("#createTypeTextDiv").addClass("hidden");
            $("#viewModeDiv").hide();

            $("#modeDiv").hide();
            
        });


    }

    function getCVByName(name) {
        $.ajax({
            type: "POST",
            url: "home.aspx/getCVByName?name=" + name,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {

                var cvs = JSON.parse(data.d);
                console.log(cvs);
                addNodes(cvs, "cv");
                

            },
            failure: function (response) {
                console.log("Failure");
            }
        });
    }

    function getCharacterByName(name) {
        $.ajax({
            type: "POST",
            url: "home.aspx/getCharacterByName?name=" + name,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
                
                var characters = JSON.parse(data.d);
                console.log(characters);
                addNodes(characters, "character");
                

            },
            failure: function (response) {
                console.log("Failure");
            }
        });
    }

    function getAnimeByName(name) {
        $.ajax({
            type: "POST",
            url: "home.aspx/getAnimeByName?name=" + name,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
                
                var animes = JSON.parse(data.d);
                console.log(animes);
                addNodes(animes, "anime");
                

            },
            failure: function (response) {

                console.log("Failure");
            }
        });
    }



    function addNodes(items, type) {
        for (i in items) {
            var item = items[i];
            var node = item;
            node.id = item.Id;
            
            node.type = type;
            nodes.push(node);
        }
        restart();
    }

    function addLinkToDatabase(link) {
        
        var character = link.source;
        var theOther = link.target;

        var url="";
        if (theOther.type == "cv") {
            url = "home.aspx/addCharacterCVRelation?cvId=" + theOther.id+"&characterId="+character.id;
        } else if (theOther.type == "anime") {
            url = "home.aspx/addCharacterAnimeRelation?animeId=" + theOther.id + "&characterId=" + character.id;
        }
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {

                var characters = JSON.parse(data.d);
                console.log(characters);
                //addNodes(characters, "character");


            },
            failure: function (response) {
                console.log("Failure");
            }
        });
    }

    function removeLinkFromDatabase(link) {
        
    }
    
    // set up SVG for D3
    var width = $(window).width(),
        height = $(window).height() - $(".container").height(),
        colors = d3.scale.category10();

    var svg = d3.select('div.visualization')
        .append('svg')
        .attr('width', width)
        .attr('height', height);
    var nodes = [];
    var links = [];
    
    // init D3 force layout
    var force = d3.layout.force()
        .nodes(nodes)
        .links(links)
        .size([width, height])
        .linkDistance(250)
        .charge(-1000)
        .on('tick', tick);

    // line displayed when dragging new nodes
    var drag_line = svg.append('svg:path')
        .attr('class', 'link dragline hidden')
        .attr('d', 'M0,0L0,0');

    // handles to link and node element groups
    var path = svg.append('svg:g').selectAll('path'),
        circle = svg.append('svg:g').selectAll('g');

    // mouse event vars
    var selected_node = null,
        selected_link = null,
        mousedown_link = null,
        mousedown_node = null,
        mouseup_node = null;

    function resetMouseVars() {
        mousedown_node = null;
        mouseup_node = null;
        mousedown_link = null;
    }

    // update force layout (called automatically each iteration)
    function tick() {
        // draw directed edges with proper padding from node centers
        path.attr('d', function (d) {
            var deltaX = d.target.x - d.source.x,
                deltaY = d.target.y - d.source.y,
                dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
                normX = deltaX / dist,
                normY = deltaY / dist,
                sourcePadding = d.left ? 17 : 12,
                targetPadding = d.right ? 17 : 12,
                sourceX = d.source.x + (sourcePadding * normX),
                sourceY = d.source.y + (sourcePadding * normY),
                targetX = d.target.x - (targetPadding * normX),
                targetY = d.target.y - (targetPadding * normY);
            return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
        });

        circle.attr('transform', function (d) {
            return 'translate(' + d.x + ',' + d.y + ')';
        });
    }

    // update graph (called when needed)
    function restart() {

        // path (link) group
        path = path.data(links);

        // update existing links
        path.classed('selected', function (d) { return d === selected_link; });


        // add new links
        path.enter().append('svg:path')
            .attr('class', 'link')
            .classed('selected', function (d) { return d === selected_link; })
            .on('mousedown', function (d) {

                // select link
                mousedown_link = d;
                if (mousedown_link === selected_link) selected_link = null;
                else selected_link = mousedown_link;
                selected_node = null;
                restart();
            });

        // remove old links
        path.exit().remove();


        // circle (node) group
        // NB: the function arg is crucial here! nodes are known by id, not by index!
        circle = circle.data(nodes, function (d) { return d.type+d.id; });

        

        d3.selectAll("circle").style('fill', function (d) {
            if (d === selected_node) {
                
                return colors(4);
            } else {
                switch (d.type) {
                    case "cv":
                        return colors(0);

                    case "character":
                        return colors(1);
                    case "anime":
                        return colors(2);
                    default:
                        return colors(3);
                }
            }
        });

        // add new nodes
        var g = circle.enter().append('svg:g');
        var canUnselect = false;
        g.append('svg:circle')
            .attr('class', 'node')
            .attr('r', 40)
            .style('fill', function (d) {
                
                
                switch (d.type) {
                    case "cv":
                        return colors(0);
                        
                    case "character":
                        return colors(1);
                    case "anime":
                        return colors(2);
                    default:
                        return colors(3);
                }
                
            })
            .style('stroke', function (d) { return "#000000";})
            .on('mousedown', function (d) {
                
                if (d3.event.ctrlKey) return;

                // select node
                mousedown_node = d;
                if (mousedown_node === selected_node) {
                    canUnselect = true;
                } else {
                    selected_node = mousedown_node;
                    canUnselect = false;
                }

                selected_link = null;

                // reposition drag line
                drag_line
                .classed('hidden', false)
                .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);

                restart();
            })
            .on('mouseup', function (d) {
                if (!mousedown_node) return;

                // needed by FF
                drag_line
                .classed('hidden', true);
                
                // check for drag-to-self
                mouseup_node = d;
                
                if (mousedown_node === selected_node && mousedown_node === mouseup_node && canUnselect) {
                    selected_node = null;
                    restart();
                }
                if (mouseup_node === mousedown_node) { resetMouseVars(); return; }

                // unenlarge target node
                d3.select(this).attr('transform', '');

                // add link to graph (update if exists)
                // NB: links are strictly source < target; arrows separately specified by booleans
                
                var character, theOther;
                
                if (mousedown_node.type == "character") {
                    character = mousedown_node;
                    theOther = mouseup_node;
                } else if (mouseup_node.type == "character") {
                    theOther = mousedown_node;
                    character = mouseup_node;
                } else {
                    return;
                }
                if (theOther.type == "character") {
                    return;
                }
                var link;
                link = links.filter(function (l) {
                    return (l.source === character && l.target === theOther);
                })[0];

                if (link) {
                    return;
                } else {

                    link = { source: character, target: theOther };
                    var existingLink = links.filter(function(l) {
                        return (link.source == l.source) && (link.target.type == l.target.type);
                    })[0];
                    console.log("found links:" + existingLink);
                    if (existingLink) {
                        links.splice(links.indexOf(existingLink), 1);
                        removeLinkFromDatabase();
                    }

                    links.push(link);
                    addLinkToDatabase(link);
                    
                }

                // select new link
                selected_link = link;
                selected_node = null;
                restart();
            })
            .on('dblclick',function(d) {
                
            })
            .on('mouseover',function(d){
                
            });
        

        // show node IDs
        g.append('svg:text')
            .attr('x', 0)
            .attr('y', 60)
            .attr('class', 'id')
            .text(function (d) { return (d.type == "cv") ? d.Name : d.name; });
        
        // remove old nodes
        circle.exit().remove();

        // set the graph in motion
        force.start();
    }

    function mousedown() {
        d3.event.preventDefault();
        // prevent I-bar on drag
        //d3.event.preventDefault();
        if (mode == "edit") {
            
            var type = $("#createType").val();
            var url = "";
            switch (type) {
            case "cv":
                url = "home.aspx/createNewCV";
                break;
            case "character":
                url = "home.aspx/createNewCharacter";
                break;
            case "anime":
                url = "home.aspx/createNewAnime";
                break;

            }
            var point = d3.mouse(this);
            if (mousedown_node || mousedown_link) return;
            $.ajax({
                type: "POST",
                url: url,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function(data) {

                    var newItem = JSON.parse(data.d);
                    console.log(newItem);
                    addNode(newItem, type, point);
                    restart();
                },
                failure: function(response) {

                    console.log("Failure");
                }
            });
        }   
    }

    function addNode(item, type, point) {
        
        
        var node = item;
        node.id = item.Id;
        node.type = type;
        node.x = point[0];
        node.y = point[1];
        nodes.push(node);
        
        restart();
    }

    function mousemove() {
        if (!mousedown_node) return;

        // update drag line
        drag_line.attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + d3.mouse(this)[0] + ',' + d3.mouse(this)[1]);

        restart();
    }

    function mouseup() {
        if (mousedown_node) {
            // hide drag line
            drag_line
                .classed('hidden', true);
        }



        // clear mouse event vars
        resetMouseVars();
    }

    function spliceLinksForNode(node) {
        var toSplice = links.filter(function (l) {
            return (l.source === node || l.target === node);
        });
        toSplice.map(function (l) {
            links.splice(links.indexOf(l), 1);
            removeLinkFromDatabase(l);
        });
    }



    function keydown() {
        //d3.event.preventDefault();
        if (!selected_node && !selected_link) return;
        switch (d3.event.keyCode) {
            case 8: // backspace
            case 46: // delete
                if (selected_node) {
                    nodes.splice(nodes.indexOf(selected_node), 1);
                    spliceLinksForNode(selected_node);
                } else if (selected_link) {
                    links.splice(links.indexOf(selected_link), 1);
                }
                selected_link = null;
                selected_node = null;
                restart();
                break;
        }
    }


    // app starts here
    svg.on('mousedown', mousedown)
        .on('mousemove', mousemove)
        .on('mouseup', mouseup);
    d3.select(window)
        .on('keydown', keydown);
    restart();

    
});



