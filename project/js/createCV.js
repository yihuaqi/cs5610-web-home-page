﻿$(function () {
    var imgs = new Array();
    $(".addImageBtn").click(addImg);

    $(".flexslider").mouseenter(function () {
        $(".deleteBtn").removeClass("inactive");
    }).mouseleave(function () {
        $(".deleteBtn").addClass("inactive");
    });

    $(".deleteBtn").click(deleteImg);

    function deleteImg() {
        var currentSlide = $('.flexslider').data('flexslider').currentSlide;
        $('.flexslider').data('flexslider').removeSlide(currentSlide);
    }

    function addImg() {

        var imgURL = $(".image-url").val();
        var imgLi = $("<li>").append(($("<img />").attr({ "src": imgURL }).addClass("portrait selectDisable cv-img")));
        var slider = $('.flexslider').data('flexslider');
        slider.addSlide($(imgLi));

        var imgSrcs = "";
        $(".cv-img").each(function () {

            imgSrcs += $(this).attr('src') + "???";
        });

        $("#cv_images_input").val(imgSrcs);
        console.log($("#cv_images_input").val());
    }


    $('.flexslider').flexslider({
        //We must have these features to correctly add new slide. Magic.
        animation: "slide",
        animationLoop: false,
    });



});