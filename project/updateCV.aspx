﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="updateCV.aspx.cs" Inherits="project_createCV" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../js/jquery-1.11.1.js"></script>
    <script src="../js/jquery-ui.js"></script>

    <link href="../css/jquery-ui.structure.css" rel="stylesheet" />

    <script src="js/createCV.js"></script>
    <link href="../css/bootstrap.css" rel="stylesheet" />
    
    <link href="css/createCV.css" rel="stylesheet" />
    <script src="../js/jquery.flexslider.js"></script>
    <link rel="stylesheet" href="../css/flexslider.css" type="text/css" />
</head>
<body>
        <div class="portrait-wrapper">
            <div class="flexslider">

                <ul class="slides">
                    <!-- We must need a placeholder here to be able to add image. Magic. -->
                    <li>
                        <%--<img class="portrait selectDisable cv-img" src="http://upload.wikimedia.org/wikipedia/commons/a/a1/Kana_Hanazawa.jpg" />--%>
                                
                    </li>

                </ul>
                        
                        

                <!-- A delete button for deleting the current image. -->
                <img class="deleteBtn inactive selectDisable" src="../../images/project/delete.png" />
            </div>
            URL<input class="image-url form-control"/>
            <button class="btn btn-success addImageBtn">Add</button>
                    
        </div>

        <form id="form1" runat="server">

            <input type="hidden" id="cv_images_input" name="cv_images_input" value="" />
                <div class="voiceActress-description">
                
                    <p>
                        <span class="description-title">Name:</span> <span id="cv-name" class="description-content" contenteditable="true">Kana Hanazawa</span>
                        <asp:TextBox ID="cv_name_textbox" runat="server"></asp:TextBox>
                    </p>

                    
                    <hr />
                    <p>
                        <span class="description-title">Gender:</span> <span id="cv-gender" class="description-content" contenteditable="true">Female</span>
                        <asp:TextBox ID="cv_gender_textbox" runat="server"></asp:TextBox>
                    </p>
                    
                    <hr />
                    <p>
                        <span class="description-title">Age:</span> <span id="cv-age" class="description-content" contenteditable="true">28</span>
                        <asp:TextBox ID="cv_age_textboxp" runat="server"></asp:TextBox>
                    </p>
                    
                    <hr />
                    <p>
                        <span class="description-title">Description:</span><span id="cv-description" class="description-content" contenteditable="true">
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                            non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                        </span>
                        <asp:TextBox ID="cv_description_textbox" runat="server"></asp:TextBox>
                    </p>
                    
                </div>
            <asp:Button ID="cv_submit_btn" runat="server" Text="Submit" OnClick="cv_submit_btn_Click" />
    </form>
</body>
</html>
