﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Web.Script.Serialization;
public partial class home : System.Web.UI.Page
{
    const string TYPE_CV = "cv";
    const string TYPE_ANIME = "anime";
    const string TYPE_CHARACTER = "character";
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }


    [System.Web.Services.WebMethod]
    public static string getCVByName() 
    {

        string name = HttpContext.Current.Request.Params["name"];
        
        using (var context = new yihuaqiEntities())
        {   

            var cvs = from c in context.CV
                      where c.Name.Contains(name)
                      select new {c.Id,c.Name,c.age,c.gender,c.description };
            JavaScriptSerializer jss = new JavaScriptSerializer();
            var json = string.Empty;
            json = jss.Serialize(cvs);
            return json;
        }
        
        
    }
    [System.Web.Services.WebMethod]
    public static string getAnimeByName()
    {

        string name = HttpContext.Current.Request.Params["name"];

        using (var context = new yihuaqiEntities())
        {

            var animes = from a in context.Anime
                         where a.name.Contains(name)
                         select new { a.Id, a.name, a.type, a.description };
            JavaScriptSerializer jss = new JavaScriptSerializer();
            var json = jss.Serialize(animes);
            return json;
        }
    }
    [System.Web.Services.WebMethod]
    public static string getCharacterByName()
    {

        string name = HttpContext.Current.Request.Params["name"];

        using (var context = new yihuaqiEntities())
        {
            var characters = from c in context.Character
                             where c.name.Contains(name)
                             select new { c.Id, c.name, c.age, c.gender, c.description, c.animeId, c.cvId };
            JavaScriptSerializer jss = new JavaScriptSerializer();
            var json = jss.Serialize(characters);
            return json;
        }
    }
    [System.Web.Services.WebMethod]
    public static string createNewCV()
    {
        using (var context = new yihuaqiEntities())
        {
            CV newCV = new CV();
            context.CV.Add(newCV);
            context.SaveChanges();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            var json = jss.Serialize(newCV);
            return json;
        }
    }
    [System.Web.Services.WebMethod]
    public static string createNewCharacter()
    {
        using (var context = new yihuaqiEntities())
        {
            Character newCharacter = new Character();
            context.Character.Add(newCharacter);
            context.SaveChanges();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            var json = jss.Serialize(newCharacter);
            return json;
        }
    }
    [System.Web.Services.WebMethod]
    public static string createNewAnime()
    {
        using (var context = new yihuaqiEntities())
        {
            Anime newAnime = new Anime();
            context.Anime.Add(newAnime);
            context.SaveChanges();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            var json = jss.Serialize(newAnime);
            return json;
        }
    }
    [System.Web.Services.WebMethod]
    public static void addCharacterCVRelation()
    {
        int characterId = Convert.ToInt32(HttpContext.Current.Request.Params["characterId"]);
        int cvId = Convert.ToInt32(HttpContext.Current.Request.Params["cvId"]);
        using (var context = new yihuaqiEntities())
        {
            Character character = context.Character.SingleOrDefault(ch => ch.Id == characterId);
            character.cvId = cvId;
            context.SaveChanges();
        }
    }
    [System.Web.Services.WebMethod]
    public static void addCharacterAnimeRelation()
    {
        int characterId = Convert.ToInt32(HttpContext.Current.Request.Params["characterId"]);
        int animeId = Convert.ToInt32(HttpContext.Current.Request.Params["animeId"]);
        using (var context = new yihuaqiEntities())
        {
            Character character = context.Character.SingleOrDefault(ch => ch.Id == characterId);
            character.animeId = animeId;
            context.SaveChanges();
        }
    }

}